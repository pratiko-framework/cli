import { resolve } from 'path';
import * as commander from 'commander';

import { action, create } from '.';

describe('create as a function', () => {
  it('is defined', () => {
    expect(create).toBeDefined();
  });

  it('returns a command', () => {
    expect(create()).toBeInstanceOf(commander.Command);
  });
});

describe(`command's action`, () => {
  const mocks: any = {
    copySync: jest.fn(),
  };

  it('is defined', () => {
    expect(action).toBeDefined();
  });

  it('only accepts Meerkat or Diplomat', () => {
    expect(() => action('Foo')).toThrow();
  });

  it('only works inside a module dir', () => {
    mocks.existsSync = jest.fn().mockReturnValueOnce(false);
    expect(() => action('Diplomat', mocks)).toThrow();
  });

  it('creates a dir named as the component', () => {
    const compPath: string = resolve(process.cwd(), 'Diplomat');
    mocks.ensureDirSync = jest.fn();
    mocks.existsSync.mockReturnValueOnce(true);

    action('Diplomat', mocks);

    expect(mocks.ensureDirSync).toHaveBeenCalledWith(compPath);
  });

  it('inits a node module with dependencies', () => {
    mocks.existsSync.mockReturnValueOnce(true);
    mocks.ensureDirSync = jest.fn();
    mocks.exec = jest.fn();

    action('Diplomat', mocks);

    const devDeps: string[] = [
      '@types/jest',
      '@types/node',
      '@typescript-eslint/eslint-plugin',
      '@typescript-eslint/parser',
      'eslint',
      'eslint-config-prettier',
      'eslint-plugin-prettier',
      'jest',
      'nodemon',
      'prettier',
      'ts-jest',
      'ts-node',
      'typescript',
    ];

    const commands: string[] = [
      'cd Diplomat',
      'yarn init -y',
      'yarn add @pratiko-framework/pratiko',
      'yarn add -D ' + devDeps.join(' '),
    ];

    expect(mocks.exec).toHaveBeenCalledWith(commands.join(' && '));
  });

  it('copies toolbelt config files', () => {
    mocks.existsSync.mockReturnValueOnce(true);
    mocks.ensureDirSync = jest.fn();
    mocks.exec.mockImplementationOnce(() => {});
    mocks.copySync = jest.fn();

    const files: string[] = ['.eslintrc.js', '.prettierrc.js', 'jest.config.js', 'nodemon.json', 'tsconfig.json'];

    action('Diplomat', mocks);

    for (const file of files) {
      const configFilePath: string = resolve(__dirname, '..', 'static', file);
      const destPath: string = resolve(process.cwd(), 'Diplomat', file);
      expect(mocks.copySync).toHaveBeenCalledWith(configFilePath, destPath);
    }
  });
});
