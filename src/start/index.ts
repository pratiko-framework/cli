import { ensureDirSync, ensureFileSync } from 'fs-extra';
import { resolve } from 'path';
import { exec } from 'shelljs';
import * as commander from 'commander';

const defaultDeps: any = {
  ensureDirSync,
  ensureFileSync,
  exec,
};

export function action(name: string, deps: any = {}): void {
  const { ensureDirSync, ensureFileSync, exec }: any = {
    ...defaultDeps,
    ...deps,
  };

  const modulePath: string = resolve(process.cwd(), name);
  const configFilePath: string = resolve(modulePath, 'module.ptk.yaml');

  ensureDirSync(modulePath);
  exec(`cd ${name} && git init`);
  ensureFileSync(configFilePath);
}

export function start(): commander.Command {
  const cmd: commander.Command = new commander.Command('start');
  cmd.arguments('<name>').action(action);

  return cmd;
}
