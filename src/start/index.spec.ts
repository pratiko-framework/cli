import * as commander from 'commander';
import { resolve } from 'path';

import { action, start } from '.';

describe('start as a function', () => {
  it('is defined', () => {
    expect(start).toBeDefined();
  });

  it('returns a command', () => {
    expect(start()).toBeInstanceOf(commander.Command);
  });
});

describe(`command's action`, () => {
  it('is defined', () => {
    expect(action).toBeDefined();
  });

  it('creates the directory, inits a repo and creates config file', () => {
    const dirPath: string = resolve(process.cwd(), 'Foo');
    const configFilePath: string = resolve(dirPath, 'module.ptk.yaml');
    const mocks: any = {
      ensureDirSync: jest.fn(),
      exec: jest.fn(),
      ensureFileSync: jest.fn(),
    };

    action('Foo', mocks);

    expect(mocks.ensureDirSync).toHaveBeenCalledWith(dirPath);
    expect(mocks.exec).toHaveBeenCalledWith('cd Foo && git init');
    expect(mocks.ensureFileSync).toHaveBeenCalledWith(configFilePath);
  });
});
