import { resolve } from 'path';
import { existsSync, ensureDirSync, copySync } from 'fs-extra';
import { exec } from 'shelljs';
import * as commander from 'commander';

const defaultDeps: any = {
  existsSync,
  ensureDirSync,
  exec,
  copySync,
};

const devDeps: string[] = [
  '@types/jest',
  '@types/node',
  '@typescript-eslint/eslint-plugin',
  '@typescript-eslint/parser',
  'eslint',
  'eslint-config-prettier',
  'eslint-plugin-prettier',
  'jest',
  'nodemon',
  'prettier',
  'ts-jest',
  'ts-node',
  'typescript',
];

const commands: string[] = [
  'cd Diplomat',
  'yarn init -y',
  'yarn add @pratiko-framework/pratiko',
  'yarn add -D ' + devDeps.join(' '),
];

const files: string[] = ['.eslintrc.js', '.prettierrc.js', 'jest.config.js', 'nodemon.json', 'tsconfig.json'];

export function action(type: string, deps: any = {}): void {
  const { existsSync, ensureDirSync, exec, copySync }: any = {
    ...defaultDeps,
    ...deps,
  };

  const acceptedTypes: string[] = ['Diplomat', 'Meerkat'];

  if (!acceptedTypes.includes(type)) {
    throw new Error();
  }

  const moduleConfigPath: string = resolve(process.cwd(), 'module.ptk.yaml');
  if (!existsSync(moduleConfigPath)) {
    throw new Error();
  }

  const compPath: string = resolve(process.cwd(), type);
  ensureDirSync(compPath);

  exec(commands.join(' && '));

  for (const file of files) {
    const srcPath: string = resolve(__dirname, '..', 'static', file);
    const destPath: string = resolve(process.cwd(), type, file);
    copySync(srcPath, destPath);
  }
}

export function create(): commander.Command {
  const cmd: commander.Command = new commander.Command('create');

  cmd
    .arguments('<type>')
    .description('creates a component (accepts only Meerkat or Diplomat)')
    .action(action);

  return cmd;
}
