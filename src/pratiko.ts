import { Command } from 'commander';
import { start } from './start';
import { create } from './create';

export const pratiko = new Command();

pratiko.addCommand(start()).addCommand(create());
