import * as clc from 'cli-color';

export const success: clc.Format = clc.greenBright;
export const failure: clc.Format = clc.red;
export const warning: clc.Format = clc.yellowBright.bold;
export const info: clc.Format = clc.blueBright;
export const debug: clc.Format = clc.magenta;
