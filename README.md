# Pratiko

Pratiko - a Progressive Event-Driven Microservices Framework

```bash
pratiko start Foo
# generates a new module directory
#   - git repo
#   - module.ptk.yaml

cd Foo

echo " ... " >> module.ptk.yaml
# fulfills module.ptk.yaml with data regarding global objects
#   - broker config --> docker-compose.yaml
#   - database config
#     ---> package.json
#     ---> docker-compose.yaml
#     ---> config file name/path

pratiko create (Meerkat|Diplomat)
# creates a Component scaffold
#   - package.json
#   - dir structure
#   - dev toolbelt configs
#   - src/index.ts
#   - docker files (if module.ptk.yaml contains that data)

# IDEA:
# pratiko generate (db|docker|types)
#   docker:
#     - in case `pratiko create (Meerkat|Diplomat)`
#       is called before the module.ptk.yaml
#     - Dockerfile and docker-compose.yaml
#   db:
#     - package.json
#     - docker-compose.yaml
#     ? db.ts (setup file)
#   types:

```

## Installation

```bash
sudo yarn global add @pratiko-framework/cli
```

> You can also use NPM `sudo npm install --global @pratiko-framework/cli`

## Usage

The Pratiko-cli is a tool to help out during the early development workflow, with commands to scaffold repository and deal with chores such as dockerization.

Start a new module with:

```bash
pratiko start Foo

cd Foo
```

This will create a new repository for your module

```diff
+ Foo/
+     .git
+     module.ptk.yaml
```

At this point, we recomend you to fulfill the `module.ptk.yaml` config file with information that will be useful in the next steps. Open it and write:

```yaml
module_name: Foo

http_port: 9090

broker:
  name: broker
  port: 4222
  cluster_id: 'foo-broker'
  monitoring: false

db:
  config_file: ./db.ts
  docker_image: mongo:4.2.3-bionic
  packages:
    dependencies:
      - mongodb: ^3.5.5
    dev_dependencies:
      - '@types/mongodb': ^3.5.2
```

In the above example, we adopted MongoDB as our module's db. We set the `mongo:4.2.3-bionic` docker image to be injected in the `docker-compose.yaml` files in the components (more in the next section). We also defined the packages used, both `dependencies` and `dev_dependencies`, which will be added to the `package.json` file, accordingly. Finally, we let you configure the connection you way, informing a `config_file` to be used. That file will simply be copied to the components when you run the next command.

Now create a HTTP-responsible component, the Diplomat, with

```bash
pratiko create Diplomat
```

During the execution of this command, `pratiko` will create a directory to contain Diplomat files, such as

- `package.json`, which will be inhanced with the MongoDB dependencies
- `src/index.ts`, the entrypoint of the component
- `src/db.ts`, the given config file for the database
- `Dockerfile`, exposing the `http_port` defined (default: 9090)
- `docker-compose.yaml`, containing configuration for
  - diplomat,
  - broker,
  - db
- dev toolbelt files, a scaffold with configurations for
  - ESLint & Prettier for Linter rules
  - Jest for testing
  - Nodemon for hot reload the component

So the repository now looks like this:

```diff
Foo/
    .git
    module.ptk.yaml
+   Diplomat/
+       Dockerfile
+       docker-compose.yaml
+       .eslintrc.js
+       jest.config.js
+       nodemon.json
+       package.json
+       .prettierrc.js
+       src/
+           index.ts
+           db.ts
+       tsconfig.json
```

Now, the Diplomat is ready to receive some code! Check details in the [@pratiko-framework/pratiko guide](https://gitlab.com/pratiko-framework/pratiko).

First, check if the database was added to the context. open the `src/index.ts` and check if it matches:

```ts
import { Diplomat } from '@pratiko-framework/pratiko';
import { db } from './db';

const app: Diplomat = new Diplomat({
  broker: new Broker({
    clusterID: process.env.BROKER_CLUSTER_ID,
    clientID: process.env.BROKER_CLIENT_ID,
    url: process.env.BROKER_URL,
  }),
  resolvers: [
    // your resolvers come here
  ],
});

app.broker.on('connect', () => {
  app.context.db = db;
  app.listen(Number.parseInt(process.env.PORT));
});
```

Let's create a `Resolver` for our Diplomat. With that, we'll open an endpoint in our API. Create a file `src/resolvers/HelloWorld.ts` with

```ts
export function HelloWorld(ctx: any): void {
  ctx.body = { hello: 'world' };
}
```

And then let's use it within a resolver. Open `src/index.ts` and add

```ts
// 1. add Resolver
import { Diplomat, Resolver } from '@pratiko-framework/pratiko';
import { db } from './db';

// 2. import our function
import { HelloWorld } from './resolvers/HelloWorld';

const app: Diplomat = new Diplomat({
  broker: new Broker({
    clusterID: process.env.BROKER_CLUSTER_ID,
    clientID: process.env.BROKER_CLIENT_ID,
    url: process.env.BROKER_URL,
  }),
  resolvers: [
    // 3. create a Resolver for that function
    new Resolver({
      route: 'GET /helloworld',
      callback: HelloWorld,
    }),
  ],
});

app.broker.on('connect', () => {
  app.context.db = db;
  app.listen(Number.parseInt(process.env.PORT));
});
```

That's it! To test it, run from the Diplomat dir

```bash
docker-compose up --build
```

and open the browser into `http://localhost:9090/helloworld` and check your message!
